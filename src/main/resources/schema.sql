CREATE SCHEMA IF NOT EXISTS baseproject;
SET SCHEMA baseproject;

CREATE TABLE book(
    id bigint auto_increment,
    title VARCHAR(50) NOT NULL,
    author VARCHAR(50) NOT NULL,
    ISDB VARCHAR (50) NOT NULL
);

CREATE UNIQUE INDEX IDX_TITLE ON book(title);
CREATE INDEX IDX_AUTHOR ON book(author);
CREATE UNIQUE INDEX  IDX_ISDB ON book(ISDB);