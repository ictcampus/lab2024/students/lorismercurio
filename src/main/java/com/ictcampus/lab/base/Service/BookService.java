package com.ictcampus.lab.base.Service;

import com.ictcampus.lab.base.Service.model.Book;
import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.repository.BookRepository;
import com.ictcampus.lab.base.repository.entity.BookEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j

public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> getBooks() {
		List<Book> bookList = new ArrayList<>();

		List<BookEntity> bookEntities = bookRepository.findAll();
		for (BookEntity bookEntity : bookEntities) {
			bookList.add( convertToBook(bookEntity) );
		}
		return bookList;
	}

    public List<Book> getBooks( String title, String search ) {
		List<Book> bookList = new ArrayList<>();

		List<BookEntity> bookEntities = bookRepository.findByTitleOrSearch( title,  search);
		for (BookEntity bookEntity : bookEntities) {
			bookList.add( convertToBook(bookEntity) );
		}
		return bookList;
	}

    public Book getBookById ( Long id ) {
		return convertToBook( bookRepository.findById( id ) );
	}

    public Long addBook( String title, String author, String ISDB ) {

		return bookRepository.create(title,author,ISDB);
	}

    public void editBook( Long id, String title, String author, String ISDB ) {
		bookRepository.update( id, title, author,ISDB );
	}

    public void deleteBook( Long id ) {
        bookRepository.delete( id );
	}

    private Book convertToBook(BookEntity bookEntity) {
        Book book = new Book();
        book.setId(bookEntity.getId());
        book.setTitle(bookEntity.getTitle());
        book.setAuthor(bookEntity.getAuthor());
        book.setISDB(bookEntity.getISDB());

        return book;

    }

}
