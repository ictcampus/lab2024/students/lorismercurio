package com.ictcampus.lab.base.Service.model;

public class Book {

    private Long id;
    private String title;
    private String author;
    private String ISDB;


    public Book(Long id, String title, String author, String ISDB) {

        this.id = id;
        this.title = title;
        this.author = author;
        this.ISDB= ISDB;
    }
    public Book(){}

        public Long getId() {return id;}
	    public void setId( final Long id ) {this.id = id;}

	public String getTitle() {return title;}
	public void setTitle( final String title ) {this.title = title;}

	public String getAuthor() {return author;}
	public void setAuthor( final String author ) {this.author = author;}

	public String getISDB() {return ISDB;}
	public void setISDB( final String ISDB ) {this.ISDB = ISDB;}

    @Override
    public String toString() {
        return "Book [+" +
                "id=" + id
                + ", title=" + title + '\''+
                ", author=" + author + '\''+
                ", ISDB=" + ISDB +
                '}';

    }

    }

