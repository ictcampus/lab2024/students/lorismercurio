package com.ictcampus.lab.base.repository;

import com.ictcampus.lab.base.repository.entity.BookEntity;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import lombok.extern.slf4j.Slf4j;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
@AllArgsConstructor
@Slf4j

public class BookRepository {
    private JdbcTemplate jdbcTemplate;

    public List<BookEntity> findAll() {
        return jdbcTemplate.query("SELECT * FROM book", new BeanPropertyRowMapper<>(BookEntity.class));
    }

    public BookEntity findById(long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM book WHERE id = ?",
                new BeanPropertyRowMapper<>(BookEntity.class), id);

    }
        public BookEntity findByTitle(String title) {
            return jdbcTemplate.queryForObject("SELECT * FROM book WHERE title = ?",
                    new BeanPropertyRowMapper<>(BookEntity.class), title);
        }

       public List<BookEntity> findByTitleOrSearch(String title, String search ) {

		/*if ( (name == null || name.isEmpty()) && (search == null || search.isEmpty()) ) {
			return findAll();
		}*/

		List<String> args = new ArrayList<>();
		String sql = " SELECT * "
					 + "FROM book "
					 + "WHERE (1=1) ";

		if( title != null && !title.isEmpty() ) {
			sql += " AND (title ILIKE ?) ";
			args.add( "%" + title + "%");
		}

		if( search != null && !search.isEmpty() ) {
			sql += " AND (title ILIKE ? OR author ILIKE ?)";
			args.add( "%" + search + "%");
			args.add( "%" + search + "%");
		}


		log.info( "Ho chiamato findByNameOrSearch() e SQL generato è [{}]", sql );

		return jdbcTemplate.query( sql,
				new BeanPropertyRowMapper<>( BookEntity.class ), args.toArray( new String[0] ) );
	}

    @Transactional
    public long create(String title, String author,String ISDB) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		PreparedStatementCreatorFactory preparedStatementCreatorFactory = new PreparedStatementCreatorFactory(
				"INSERT INTO book (title, author,ISDB) VALUES (?, ?, ?)",
				Types.VARCHAR, Types.VARCHAR, Types.VARCHAR
		);
		preparedStatementCreatorFactory.setReturnGeneratedKeys( true );

		PreparedStatementCreator preparedStatementCreator = preparedStatementCreatorFactory
				.newPreparedStatementCreator(
						Arrays.asList( title, author,ISDB )
				);


		jdbcTemplate.update( preparedStatementCreator, generatedKeyHolder );

		return generatedKeyHolder.getKey().longValue();

    }

    public int update( long id, String title, String author, String ISDB) {
		return jdbcTemplate.update( "UPDATE book SET title=?, author=?, ISDB=? WHERE id=?", title, author, ISDB,id );
	}

	public int delete( long id ) {
        return jdbcTemplate.update("DELETE FROM book WHERE id=?", id);
    }

	}







