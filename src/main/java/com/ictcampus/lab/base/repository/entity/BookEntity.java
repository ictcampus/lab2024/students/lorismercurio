package com.ictcampus.lab.base.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class BookEntity {
    @Id
    private long id;

    private String title;
    private String author;
    private String ISDB;

    public Long getId() {return id;}
    public void setId( final Long id ) {this.id = id;}

	public String getTitle() {return title;}
	public void setTitle( final String title ) {this.title = title;}

	public String getAuthor() {return author;}
	public void setAuthor( final String author ) {this.author = author;}

	public String getISDB() {return ISDB;}
	public void setISDB( final String ISDB ) {this.ISDB = ISDB;}

}
