package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.Exception.NotFoundException;
import com.ictcampus.lab.base.control.model.BookRequest;
import com.ictcampus.lab.base.control.model.BookResponse;
import com.ictcampus.lab.base.Service.BookService;
import com.ictcampus.lab.base.Service.model.Book;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
    @Autowired
    private BookService bookService;


    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<BookResponse> getBooks(

      @RequestParam( required = false) String title,
      @RequestParam(required = false)String search
    ){
        List<BookResponse> bookResponses = new ArrayList<>();

        	for ( Book book : bookService.getBooks( title, search ) ) {
			bookResponses.add(convertToBookResponse( book ) );
		}
		return bookResponses;

        //return bookService.getBooks(title, search).stream().map( item -> convertToBookResponse( item ) ).collect( Collectors.toList());
    }




    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public BookResponse getBook(@PathVariable(name = "id") Long id) throws NotFoundException {
        Book book = bookService.getBookById(id);
        if (book == null) {
            log.info("Il libro con ID [{}] non è stato trovato", id);
           throw new NotFoundException();
        }

        BookResponse bookResponse = convertToBookResponse( book );
        log.info("Il libro con ID [{}] trovato [{}]", id, bookResponse);
        return bookResponse;
    }

/*
    @GetMapping(value = "/author/{author}", produces = {MediaType.APPLICATION_JSON_VALUE})

	public List<BookResponse> searchByAuthor(@PathVariable(name = "author") String author) {

        List<BookResponse> authorlist = new ArrayList<>();
        for (BookResponse book : List) {

            if (book.getAuthor().toLowerCase().contains(author.toLowerCase())) {
                authorlist.add(book);

            } else {
                log.warn("L'autore è sbagliato " + author + " not found.");
            }
        }

        return authorlist;
    }*/

    @PostMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Long createBook(
            @RequestBody BookRequest bookRequest
    ) {

        log.info("Creazione di un nuovo libro [{}]", bookRequest);

        return bookService.addBook(
                bookRequest.getTitle(),
                bookRequest.getAuthor(),
                bookRequest.getISDB()
        );
    }

    @PutMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public void editBook(
            @PathVariable(name = "id") Long id,
            @RequestBody BookRequest bookRequest
    ) {

        log.info("modifica di un libro [{}]", id, bookRequest);

        bookService.editBook(
                id,
                bookRequest.getTitle(),
                bookRequest.getAuthor(),
                bookRequest.getISDB()
        );

    }

    @DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public void deleteBook(
            @PathVariable(name = "id") Long id
    ) {
        log.info("Eliminazione di un libro [{}]", id);
        bookService.deleteBook(id);
    }



private BookResponse convertToBookResponse(Book book) {
        BookResponse bookResponse = new BookResponse ();
        bookResponse.setId(book.getId());
        bookResponse.setTitle(book.getTitle());
        bookResponse.setAuthor(book.getAuthor());
        bookResponse.setISDB(book.getISDB());
        return bookResponse;

    }
}






